import memory_profiler

# if we want to debug memory, activate this parameter
MEMORY_PROFILING = False

if MEMORY_PROFILING:

    def profile(func):
        return memory_profiler.profile(func)


else:

    def profile(func):
        return func
