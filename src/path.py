import os

import src.constants as constants

DATA_DIR = os.getenv("DATA_DIR", "data")
os.makedirs(DATA_DIR, exist_ok=True)
DATA_DIR_ENV = os.path.join(DATA_DIR, constants.CURRENT_ENVIRONMENT)
os.makedirs(DATA_DIR_ENV, exist_ok=True)


def path_artifact(dataset_name: str) -> str:
    """Return path to artifact (a file which is dependant of the CURRENT_ENVIRONMENT (demo,
    preprod, prod))

    Args:
        dataset_name (str): filename with extension

    Returns:
        str: filepath according to CURRENT_ENVIRONMENT
    """
    return os.path.join(DATA_DIR_ENV, dataset_name)
