import os
from typing import Type

import pandas as pd

import src.constants as constants
import src.model as model
import src.path as path
from src.logger import LOGGER


def read_questions() -> pd.DataFrame:
    """Read the training questions and their answers. Merge these datasets. It creates
    our "base" dataset for the project.

    Also sample the dataset according to constants.SAMPLING_FACTOR.

    Dump dataset to a parquet file to speed-up iterations (avoid recreating it
    everytime and waiting the big CSV file to be read).

    Returns:
        pd.DataFrame: [description]
    """
    if not os.path.exists(path.path_artifact("questions.parquet")):
        answers = pd.read_csv("data/answers_train.csv")
        answers = answers[list(answers.columns)[1:]]
        answers["date"] = pd.to_datetime(answers["date"])
        answers = answers.rename(columns={"date": "date_answer", "text": "text_answer"})

        questions = pd.read_csv("data/questions_train.csv")
        questions = questions[list(questions.columns)[2:]]
        questions["date"] = pd.to_datetime(questions["date"])
        questions = questions.rename(
            columns={"date": "date_question", "text": "text_question"}
        )
        assert questions["question_id"].is_unique, "We have duplicate question_id"

        questions = pd.merge(questions, answers, on=["question_id"])

        # TODO later: we are lossing valuable insight. User's answer which were not
        # accepted. We could create on training set on accepted answer but create
        # user's embedding on all their answers (independantly if they were accepted)
        # This part should be improved later on
        is_accepted = questions["answer_id"] == questions["accepted_answer_id"]
        user_notnull = questions["user_id"].notnull()
        questions = questions[is_accepted & user_notnull]
        questions["user_id"] = questions["user_id"].astype(int)
        if constants.SAMPLING_FACTOR != 0:
            # iterate faster
            LOGGER.info(f"SAMPLING_FACTOR on questions: {constants.SAMPLING_FACTOR}")
            # How to sample smartly is an interesting question. Sample by user makes
            # the problem easier
            questions = questions[
                questions["user_id"].mod(constants.SAMPLING_FACTOR) == 0
            ]

        questions["date_answer_date"] = questions["date_answer"].dt.date

        questions.to_parquet(path.path_artifact("questions.parquet"))

    else:
        questions = pd.read_parquet(path.path_artifact("questions.parquet"))

    return questions


def read_questions_test() -> pd.DataFrame:
    """Read test questions which we need to predict in production

    Returns:
        pd.DataFrame: questions_test
    """
    questions_test = pd.read_csv("data/questions_test.csv", nrows=1_000)
    questions_test = questions_test[list(questions_test.columns)[1:]]
    return questions_test


def read_users() -> pd.DataFrame:
    """Read the users dataset.

    WARNING: The user dataset is not an historized dataset.
    We can assume some columns don't change much over time such a "display_name",
    "about_me", and can be considered as "cold data". Others like "reputation",
    "up_votes", "views", changes each time a user answer a new question. Using this
    features would create a "data-leak", hence it should be avoided.

    Note: We can even argue that users start answering question BEFORE completing their
    profile.

    Returns:
        pd.DataFrame: users
    """
    users = pd.read_csv("data/users.csv", nrows=10_000)
    users = users[list(users.columns)[1:]]
    return users


def get_valid_and_predict(
    questions: pd.DataFrame, my_model: Type[model.BaseModel]
) -> pd.DataFrame:
    """We consider this problem as a timeseries one and more representative of future
    performance (questions are answered over time, you can't use user's answer from
    the future to answer the current one, otherwise it would be a data leak).
    Hence a need a train/validation set based on time.

    Args:
        questions (pd.DataFrame): question dataset (the training one)
        my_model (Type[model.BaseModel]): model will be use

    Returns:
        pd.DataFrame: validation set without any data-leak and predictions.
    """
    # We consider all dates exists
    l_dates_valid = list(questions["date_answer_date"].unique())
    l_dates_valid.sort()
    l_dates_valid = l_dates_valid[-7:]
    valid = []
    for date in l_dates_valid:
        LOGGER.info(f"Create valid set for date {date}")
        # All the historical questions available BEFORE we make a predictions
        historical_questions = questions[questions["date_answer_date"] < date]
        my_model.train(historical_questions)

        # Questions we try to predict (our validation set)
        questions_valid = questions[questions["date_answer_date"] == date]

        tmp_valid = my_model.predict(questions_valid, historical_questions)

        valid.append(tmp_valid)

    valid = pd.concat(valid)
    valid["target"] = valid["user_id"] == valid["user_id_candidate"]
    return valid


def format_submission(submission: pd.DataFrame) -> pd.DataFrame:
    """Format submission file at the desired format (logn to wide).

    Args:
        submission (pd.DataFrame): not correctly formatted

    Returns:
        pd.DataFrame: correctly formatted
    """
    submission = submission[submission["pred_rank"] <= 20].copy()
    submission = submission[["question_id", "pred_rank", "user_id_candidate"]]
    submission["pred_rank"] = submission["pred_rank"].astype(int).astype(str)
    submission["pred_rank"] = "user" + submission["pred_rank"] + "_id"
    submission = pd.pivot_table(
        submission,
        values="user_id_candidate",
        index="question_id",
        columns=["pred_rank"],
        aggfunc="first",
    )
    submission = submission.reset_index()
    return submission
