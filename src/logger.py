import logging

logging.basicConfig(
    format="%(asctime)s %(levelname)-2s: %(message)s",
    datefmt="%Y-%m-%d %H:%M:%S",
    level=logging.INFO,
)


LOGGER = logging.getLogger()
