import mlflow
import pandas as pd

from src.logger import LOGGER


def log_metrics(model_name: str, valid: pd.DataFrame) -> None:
    """Given a dataset including predictions, compute the desired metric (precision@K,
    recall@K, MRR) and log them to MLflow.

    Args:
        model_name (str): Name of our model
        valid (pd.DataFrame): a validation set with predictions
    """
    N_questions = valid["question_id"].nunique()

    metrics = {}
    for K in [1, 5, 20]:
        metrics[f"precision_at_{K}"] = (
            (valid["pred_rank"] <= K) * valid["target"]
        ).mean()

    for K in [1, 5, 20]:
        metrics[f"recall_at_{K}"] = (
            (valid["pred_rank"] <= K) * valid["target"]
        ).sum() / N_questions

    metrics["mean_reciprocal_rank"] = (
        1 / valid["pred_rank"] * valid["target"]
    ).sum() / N_questions

    def round_sig(x, sig=2):
        from math import floor, log10

        if x != 0:
            return round(x, sig - int(floor(log10(abs(x)))) - 1)
        else:
            return x

    LOGGER.info("Metrics computed:")
    for metric_name, value in metrics.items():
        LOGGER.info(f"{metric_name}: {round_sig(value, 4)}")

    mlflow.set_experiment("train")
    with mlflow.start_run():
        mlflow.log_param("model_name", model_name)
        mlflow.log_metrics(metrics)
    mlflow.end_run()


def get_previous_experiments(LAST_K):
    from mlflow.tracking import MlflowClient

    client = MlflowClient()
    l_experiments = client.search_runs(
        experiment_ids=client.get_experiment_by_name("train").experiment_id
    )

    l_experiments = l_experiments[:LAST_K]

    experiments = []
    for exp in l_experiments:
        tmp = pd.DataFrame.from_dict(dict(exp.data.metrics), orient="index").T
        tmp["model_name"] = exp.data.params["model_name"]
        experiments.append(tmp)

    experiments = pd.concat(experiments)
    cols = (
        ["model_name", "mean_reciprocal_rank"]
        + [f"recall_at_{i}" for i in [1, 5, 20]]
        + [f"precision_at_{i}" for i in [1, 5, 20]]
    )

    experiments = (
        experiments[cols].round(4).sort_values("mean_reciprocal_rank", ascending=False)
    )

    return experiments
