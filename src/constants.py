import os

# Our environement of work. Ordered by speed of iteration (due to sampling):
# demo > preprod > prod
CURRENT_ENVIRONMENT = os.getenv("CURRENT_ENVIRONMENT", "preprod")
assert CURRENT_ENVIRONMENT in ("demo", "preprod", "prod")

SAMPLING_FACTOR = {"demo": 100, "preprod": 10, "prod": 0}
SAMPLING_FACTOR = SAMPLING_FACTOR[CURRENT_ENVIRONMENT]
