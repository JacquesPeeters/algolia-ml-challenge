# import numpy as np
from abc import ABC, abstractmethod

import pandas as pd
from annoy import AnnoyIndex

import src.model_embeddings as model_embeddings
import src.model_second as model_second
from src.my_memory_profiler import profile


class BaseModel(ABC):
    """An abstract base model with train/predict method"""

    # @property
    # def strategy_name(self):
    #     raise NotImplementedError
    # TODO I should learn how to define abstract attribute :)
    def __init__(self):
        self.model_name = ""

    @abstractmethod
    def train(questions):
        pass

    @abstractmethod
    def predict(self, questions, *unused):
        pass


class NearestNeighborsModel(BaseModel):
    """NearestNeighborsModel is a model based on a-kNN candidate retrieval. Then
    candidates are ranked by distance"""

    def __init__(self):
        # TODO I should learn how to define abstract attribute :)
        # self.model_embeddings = TFIDF_Embeddings()
        # self.model_name = "TFIDF"
        # self.N_CANDIDATES = 20
        pass

    def clean_text(x):
        """TODO Not implemented yet. Should improve the quality of the embeddings.
        But it is 1am"""
        return x

    @profile
    def index_users(self, users_answers: pd.DataFrame) -> None:
        """Given a dataframe of [user_id, text]:
        * train an embedding model
        * create embedding for each users
        * insert them inside an AnnoyIndex

        Annoy is an Approximate Nearest Neighbors library.

        Args:
            users_answers (pd.DataFrame): users_answers
        """
        # index users, by building embedding then dumping them into an AnnoyIndex
        self.model_embeddings.train(users_answers["title"])

        embeddings = self.model_embeddings.predict(users_answers["title"])

        # As written in the docs:
        # Note that it will allocate memory for max(id)+1 items because it assumes your
        # items are numbered 0 … n-1. If you need other id's, you will have to keep
        # track of a map yourself.
        # Replace user_id with user_id from 1 to N to reduce memory footprint

        self.mapping_user_id_to_int = {
            k: v for v, k in enumerate(users_answers["user_id"].unique())
        }
        self.mapping_int_to_user_id = {
            v: k for k, v in self.mapping_user_id_to_int.items()
        }

        self.annoy_index = AnnoyIndex(len(embeddings[0]), "euclidean")
        for user_id, embedding in zip(users_answers["user_id"].values, embeddings):
            # Size of the index is the memory bottleneck (found via memory profiling)
            self.annoy_index.add_item(self.mapping_user_id_to_int[user_id], embedding)

        self.annoy_index.build(10)

    def get_candidates(self, questions: pd.DataFrame) -> pd.DataFrame:
        """Generates candidates for questions dataset and return our "final" dataset.
        Final dataset can be learning/train/valid/test/prod dataset.
        Steps includes:
        * create embedding of the questions
        * find user_id having the closest embeddings
        * format dataset

        Final dataset is self.N_CANDIDATES times bigger than the initial one (a
        line is created for each candidate)

        Args:
            questions (pd.DataFrame): question

        Returns:
            pd.DataFrame: final dataset
        """
        embeddings = self.model_embeddings.predict(questions["title"])

        l_nns = []
        for embedding in embeddings:
            nns = self.annoy_index.get_nns_by_vector(
                embedding, self.N_CANDIDATES, include_distances=True
            )
            l_nns.append(nns)

        questions[["user_id_candidate", "distance"]] = l_nns
        final = questions.explode(["user_id_candidate", "distance"])
        final["user_id_candidate"] = final["user_id_candidate"].map(
            self.mapping_int_to_user_id
        )
        final["distance"] = final["distance"].astype(float)
        return final

    def train(self, questions: pd.DataFrame) -> None:
        """Train our algorithm single/first stage algorithm

        Args:
            questions (pd.DataFrame): historical questions with answers
        """
        # We need to embbed our users. We will base our embeddings on the
        # questions previously answered for each users
        users_answers = (
            questions.groupby("user_id")["title"]
            .apply(list)
            .apply(lambda x: " ".join(x))
            .reset_index()
        )
        self.index_users(users_answers)

    def predict(self, questions: pd.DataFrame, *unused) -> pd.DataFrame:
        """Predict questions (returns prediction and ranks)

        Args:
            questions (pd.DataFrame): questions

        Returns:
            pd.DataFrame: [description]
        """
        final = self.get_candidates(questions)

        # We expect higher the better so keep this convention
        final["pred"] = -final["distance"]
        final["pred_rank"] = final.groupby("question_id")["pred"].rank(
            ascending=False,
            method="first",  # in case of ties
        )
        return final


class TFIDF(NearestNeighborsModel):
    """NearestNeighborsModel model based on model_embeddings.TFIDF_Embeddings()"""

    def __init__(self, N_CANDIDATES):
        self.model_embeddings = model_embeddings.TFIDF_Embeddings()
        self.model_name = "TFIDF"
        self.N_CANDIDATES = N_CANDIDATES


class TFIDF_SVD(NearestNeighborsModel):
    """NearestNeighborsModel model based on model_embeddings.TFIDF_SVD_Embeddings()"""

    def __init__(self, N_CANDIDATES):
        self.model_embeddings = model_embeddings.TFIDF_SVD_Embeddings()
        self.model_name = "TFIDF_SVD"
        self.N_CANDIDATES = N_CANDIDATES


class SentenceTransformer(NearestNeighborsModel):
    """NearestNeighborsModel model based on
    model_embeddings.SentenceTransformer_Embeddings()"""

    def __init__(self, N_CANDIDATES):
        self.model_embeddings = model_embeddings.SentenceTransformer_Embeddings()
        self.model_name = "SentenceTransformer"
        self.N_CANDIDATES = N_CANDIDATES


class TwoStepModel(BaseModel):
    """TwoStepModel model is based on NearestNeighborsModel for candidate retrieval
    then on another more classical model to "rerank" these candidates. This second
    model can take as input more features (eg user recency, ...)"""

    def __init__(self):
        # self.annoy_index = None
        # self.tfidf_vectorizer = None
        # self.model_name = "TFIDF_Annoy_LGBM"
        # self.first_model = TFIDFAnnoy()
        # self.second_model = LGBM_custom()
        pass

    @staticmethod
    def get_users_fe(historical_questions: pd.DataFrame) -> pd.DataFrame:
        """Compute various features about each user_id

        Args:
            historical_questions (pd.DataFrame): historical_questions

        Returns:
            pd.DataFrame: users_fe
        """
        fe_users = historical_questions.copy()
        fe_users["count"] = 1

        agg_func = {
            "count": ["sum"],
            "date_answer": ["max"],
        }

        cols_group = ["user_id"]
        fe_users = fe_users.groupby(cols_group).agg(agg_func)
        group_name = "_&_".join([col.upper() for col in cols_group])
        fe_users.columns = [
            group_name + "_" + "_".join(col) for col in fe_users.columns
        ]
        fe_users = fe_users.reset_index()
        fe_users = fe_users.rename(columns={"user_id": "user_id_candidate"})
        return fe_users

    @staticmethod
    def contextual_features(final: pd.DataFrame) -> pd.DataFrame:
        """Contextual: Features which depends on the current question and the candidate
        Can't be pre-computed but is stateless.

        Args:
            final (pd.DataFrame): final

        Returns:
            pd.DataFrame: final
        """
        final["days_from_last_answer"] = (
            pd.to_datetime(final["date_answer_date"]) - final["USER_ID_date_answer_max"]
        ) / pd.to_timedelta(1, unit="D")
        return final

    @staticmethod
    def group_candidates_features(final: pd.DataFrame) -> pd.DataFrame:
        """Features with depends of other candidates (group)
        Can not be pre-computed and is statefull.

        Args:
            final (pd.DataFrame): final

        Returns:
            pd.DataFrame: final
        """
        # Create ranking features to help the 2 stage model
        grouped = final.groupby(["question_id"])

        def create_ranking_fe(final, grouped, col, lower_is_better):
            final[f"{col}_rank"] = grouped[col].rank(
                ascending=lower_is_better,
                # na_option='keep',
            )

            min_ = grouped[col].transform(min)
            max_ = grouped[col].transform(max)

            final[f"{col}_minmax_scaling"] = (final[col] - min_) / (max_ - min_)

            return final

        cols_to_rank = [
            ("distance", True),
            ("USER_ID_count_sum", False),
            ("days_from_last_answer", True),
        ]

        for col, lower_is_better in cols_to_rank:
            final = create_ranking_fe(final, grouped, col, lower_is_better)

        return final

    def prepare_final(
        self, questions: pd.DataFrame, historical_questions: pd.DataFrame
    ) -> pd.DataFrame:
        """Prepare the final dataset
        Final dataset can be learning/train/valid/test/prod dataset.

        Args:
            questions (pd.DataFrame): questions
            historical_questions (pd.DataFrame): historical_questions

        Returns:
            pd.DataFrame: final
        """
        # final dataset is a dataset ready to be be fed for our second step model
        # final dataset can be learning, train, valid, prod, test, ...

        fe_users = self.get_users_fe(historical_questions)

        final = self.first_model.get_candidates(questions)
        final = pd.merge(
            final,
            fe_users,
            how="left",
        )

        final = self.contextual_features(final)

        final = self.group_candidates_features(final)

        return final

    def get_valid(self, questions: pd.DataFrame) -> pd.DataFrame:
        """A two stage/step model needs a training/validation set to train the model of
        the second step. This function creates this dataset.

        Args:
            questions (pd.DataFrame): questions

        Returns:
            pd.DataFrame: a training/validation set
        """
        # A two-step model need to create a validation set for it-self in order
        # to train the second algo

        # We consider all dates exists
        l_dates_valid = list(questions["date_answer_date"].unique())
        l_dates_valid.sort()
        l_dates_valid = l_dates_valid[-7:]
        valid = []
        for date in l_dates_valid:
            # All the historical questions available BEFORE we make a predictions
            historical_questions = questions[questions["date_answer_date"] < date]
            self.first_model.train(historical_questions)

            # Questions we try to predict (our validation set)
            questions_valid = questions[questions["date_answer_date"] == date]

            tmp_valid = self.prepare_final(questions_valid, historical_questions)

            valid.append(tmp_valid)

        valid = pd.concat(valid)
        return valid

    def train(self, questions: pd.DataFrame) -> None:
        """Train our model

        Args:
            questions (pd.DataFrame): questions with answer (train)
        """
        if not self.second_model.is_already_trained():
            # See why in func documentation
            valid = self.get_valid(questions)
            valid["target"] = valid["user_id"] == valid["user_id_candidate"]
            self.second_model.train(valid)

        # Retrain the first_model on full data
        self.first_model.train(questions)

    def predict(
        self, questions: pd.DataFrame, historical_questions: pd.DataFrame
    ) -> pd.DataFrame:
        """Predict questions

        Args:
            questions (pd.DataFrame): questions
            historical_questions (pd.DataFrame): historical_questions

        Returns:
            pd.DataFrame: predicted questions
        """
        final = self.prepare_final(questions, historical_questions)
        final["pred"] = self.second_model.predict(final)
        final["pred_rank"] = final.groupby("question_id")["pred"].rank(
            ascending=False,
            method="first",  # in case of ties
        )
        return final


class TFIDF_SVD_LGBM(TwoStepModel):
    """A TwoStepModel model based on TFIDF_SVD and model_second.LGBM_custom"""

    def __init__(self):
        self.annoy_index = None
        self.tfidf_vectorizer = None
        self.model_name = "TFIDF_SVD_LGBM"
        self.first_model = TFIDF_SVD(N_CANDIDATES=100)
        self.second_model = model_second.LGBM_custom(retrain_daily=False)


class SentenceTransformer_LGBM(TwoStepModel):
    """A TwoStepModel model based on SentenceTransformer and model_second.LGBM_custom"""

    def __init__(self):
        self.annoy_index = None
        self.tfidf_vectorizer = None
        self.model_name = "SentenceTransformer_LGBM"
        self.first_model = SentenceTransformer(N_CANDIDATES=100)
        self.second_model = model_second.LGBM_custom(retrain_daily=False)
