"""
Module where models for creating embeddings are defined (used for the candidates
retrieval step). These embeddings models are called from model.py
"""
from sentence_transformers import SentenceTransformer
from sklearn.decomposition import TruncatedSVD
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.pipeline import Pipeline


class TFIDF_Embeddings:
    """Apply a TFIDF to create embeddings."""

    def __init__(self):
        self.MAX_FEATURES = 25  # Can create memory errors if too big
        self.vectorizer = TfidfVectorizer(max_features=self.MAX_FEATURES)

    def train(self, x):
        self.vectorizer.fit(x)

    def predict(self, x):
        embeddings = self.vectorizer.transform(x)
        embeddings = embeddings.todense()
        embeddings = embeddings.tolist()
        return embeddings


class TFIDF_SVD_Embeddings:
    """Apply a TFIDF then a SVD to move from sparse to dense embeddings and reduce
    dimension. Enhancement of the TFIDF_Embeddings
    """

    def __init__(self):
        self.MAX_FEATURES = 25  # Can create memory errors if too big
        self.MAX_FEATURES_TFIDF = 10_000  # Can be improved later

        self.vectorizer = Pipeline(
            [
                ("tfidf", TfidfVectorizer(max_features=self.MAX_FEATURES_TFIDF)),
                ("svd", TruncatedSVD(n_components=self.MAX_FEATURES, random_state=1)),
            ]
        )

    def train(self, x):
        self.vectorizer.fit(x)

    def predict(self, x):
        embeddings = self.vectorizer.transform(x)
        embeddings = embeddings.tolist()
        return embeddings


class SentenceTransformer_Embeddings:
    """Apply a more "state of the art" embedding method based on
    DeepLearning/Transformers"""

    def __init__(self):
        # https://www.sbert.net/docs/pretrained_models.html
        self.vectorizer = SentenceTransformer("paraphrase-MiniLM-L6-v2")

    def train(self, x):
        pass
        # self.vectorizer.fit(x)

    def predict(self, x):
        embeddings = self.vectorizer.encode(x.values)
        embeddings = embeddings.tolist()
        return embeddings
