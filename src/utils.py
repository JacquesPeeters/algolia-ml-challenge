import functools
import time

from src.logger import LOGGER


def timer(func):
    @functools.wraps(func)
    def wrapper_timer(*args, **kwargs):
        tic = time.perf_counter()
        value = func(*args, **kwargs)
        toc = time.perf_counter()
        elapsed_time = toc - tic
        LOGGER.info(f"{func.__name__}: Elapsed time: {elapsed_time:0.1f} seconds")
        return value

    return wrapper_timer
