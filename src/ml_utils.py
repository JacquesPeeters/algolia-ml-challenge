from typing import Callable, Union

import lightgbm as lgb
import numpy as np
import pandas as pd
import plotnine as pn
import scipy
from mizani.formatters import percent_format
from plotnine import aes, ggplot
from scipy.sparse import vstack
from sklearn.model_selection import train_test_split


def get_importance_lgb(model_gbm: lgb.Booster) -> pd.DataFrame:
    """Compute feature importance of a LightGBM model

    Args:
        model_gbm (lgb.Booster): lgb model

    Returns:
        pd.DataFrame: feature importance
    """
    importance = pd.DataFrame()
    importance["feature"] = model_gbm.feature_name()
    importance["importance"] = model_gbm.feature_importance(importance_type="gain")
    importance["importance"] = (
        importance["importance"] / importance["importance"].replace(np.inf, 0).sum()
    )
    importance["importance"] = importance["importance"] * 100
    importance["importance_rank"] = importance["importance"].rank(ascending=False)
    importance = importance.sort_values("importance_rank").round(2)
    return importance


def plot_importance_lgb(importance: pd.DataFrame, max_features=30) -> pn.ggplot:
    """Plot feature importance

    Args:
        importance (pd.DataFrame): feature importance dataframe
        max_features (int, optional): Number of features to plot. Defaults to 30.

    Returns:
        pn.ggplot.ggplot: Feature importance plot
    """
    importance = importance.head(max_features).copy()  # Do not erase the given dataset
    importance["importance"] = importance["importance"] / 100
    importance["feature"] = pd.Categorical(
        importance["feature"], importance["feature"][::-1], ordered=True
    )
    plot = (
        ggplot(importance, aes("feature", "importance"))
        + pn.geom_bar(stat="identity")
        + pn.coords.coord_flip()
        + pn.scales.scale_y_continuous(labels=percent_format())
        + pn.labs(title="Feature importance", x="Feature", y="Gain")
        + pn.theme(figure_size=(9.5, 7.2))
    )
    return plot


def target_encoding(data: pd.DataFrame, cols_group: list, target: str) -> pd.DataFrame:
    """Compute target encoding (mean and count values).

    Args:
        data (pd.DataFrame): input dataframe
        cols_group (list): [description]
        target (str): column for which we want to perform target encoding

    Returns:
        pd.DataFrame: Aggregated dataframe with mean and count for each groups
    """
    # Remove line where target is Na otherwise count values wouldn't be good
    data = data[~data[target].isnull()].copy()
    data[cols_group] = data[cols_group].fillna("dont_drop_na")
    agg_func = {target: ["mean", "size"]}
    res = data.groupby(cols_group).agg(agg_func)
    group_name = "_&_".join([col.upper() for col in cols_group])
    res.columns = [group_name + "_" + "_".join(col) for col in res.columns]
    res = res.reset_index()
    res = res.replace("dont_drop_na", np.nan)
    return res


def oof_dataframe(
    data: pd.DataFrame, col_fold: str, function: Callable, **kwargs
) -> pd.DataFrame:
    """Apply a function to a dataframe in an out-of-fold manner.

    Args:
        data (pd.DataFrame): input dataframe
        col_fold (str): fold columns
        function (Callable): function to be applied

    Returns:
        pd.DataFrame: result dataframe with function applied in a out-of-fold manner
    """

    folders = data[col_fold].unique()
    res = pd.DataFrame()
    for f in folders:
        res_tmp = function(data[data[col_fold] != f].copy(), **kwargs)
        res_tmp[col_fold] = f
        res = res.append(res_tmp)

    return res


def adversarial_validation(
    X_train: Union[pd.DataFrame, scipy.sparse.csr.csr_matrix],
    X_test: Union[pd.DataFrame, scipy.sparse.csr.csr_matrix],
) -> lgb.Booster:
    """Train a lightgbm model to try identifying differences (leak) between a
    training and test set.

    If AUC is ~0.5, both dataset have the same distribution. If AUC ~1 there is
    a problem. To debug just look at feature importance of the trained model
    and identify the problematic feature.

    Args:
        X_train (Union[pd.DataFrame, scipy.sparse.csr.csr_matrix]): Training set
        X_test (Union[pd.DataFrame, scipy.sparse.csr.csr_matrix]): Test set

    Returns:
        lgb.Booster: lgb model
    """
    if isinstance(X_train, pd.DataFrame):
        X_full = X_train.append(X_test)
    if isinstance(X_train, scipy.sparse.csr.csr_matrix):
        X_full = vstack([X_train, X_test], format="csr")

    y_full = np.concatenate((np.ones(X_train.shape[0]), np.zeros(X_test.shape[0])))

    X_train, X_valid, y_train, y_valid = train_test_split(
        X_full, y_full, random_state=1
    )

    param = {"objective": "binary", "metric": "auc", "learning_rate": 0.01}

    lgb_train = lgb.Dataset(X_train, label=y_train)
    lgb_valid = lgb.Dataset(X_valid, label=y_valid)

    model_gbm = lgb.train(
        param,
        lgb_train,
        200,
        valid_sets=[lgb_train, lgb_valid],
        early_stopping_rounds=10,
        verbose_eval=10,
    )

    return model_gbm
