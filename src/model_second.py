"""
Module where models for second stage are defined. These second stage models are called
from model.py
"""

import lightgbm as lgb
import numpy as np
import pandas as pd
from sklearn.model_selection import GroupKFold

from src.logger import LOGGER


class LGBM_custom:
    """Custom LGBM wrapper specific for our use-case

    Args:
        retrain_daily (bool): Whether we want to retrain daily our model. Features
        needs to be updated/retrained daily but the model don't need to.
    """

    def __init__(self, retrain_daily: bool):
        self.model_lgb = None
        self.X_cols = []
        self.retrain_daily = retrain_daily

        self.param = {
            "objective": "lambdarank",
            "metric": ["ndcg"],
            "eval_at": 5,
            "random_state": 1,
            "verbosity": -1,
        }

    def is_already_trained(self) -> bool:
        return (not self.retrain_daily) and (self.model_lgb is not None)

    def train(self, valid: pd.DataFrame) -> None:
        """Train our custom LGBM model

        Args:
            valid (pd.DataFrame): training set
        """
        if self.is_already_trained():
            return

        target = "target"
        to_drop = [
            "question_id",
            "owner_user_id",
            "title",
            "text_question",
            "date_question",
            "accepted_answer_id",
            "answer_id",
            "user_id",
            "text_answer",
            "date_answer",
            "score",
            "date_answer_date",
            "user_id_candidate",
            "target",
            "pred",
            "USER_ID_date_answer_max",
        ]
        X_cols = list(valid.drop(columns=to_drop + [target], errors="ignore"))
        self.X_cols = X_cols

        # We need a sorted dataset, so be defensive and do it here
        valid = valid.sort_values("question_id")

        # GroupKFold then training on only one fold is equivalent to a
        # 50/50 train_test_split (based on group)
        group_kfold = GroupKFold(n_splits=2)
        for fold, (train_index, valid_index) in enumerate(
            group_kfold.split(valid, groups=valid["question_id"].mod(2))
        ):
            if fold != 0:
                break

            train_tmp = valid.iloc[train_index]
            valid_tmp = valid.iloc[valid_index]
            X_train, y_train = train_tmp[X_cols], train_tmp[target]
            X_valid, y_valid = valid_tmp[X_cols], valid_tmp[target]
            assert y_train.mean() > 0
            LOGGER.info(f"Train LGB on {y_train.sum()} positive examples")
            LOGGER.info(f"Validate LGB on {y_valid.sum()} positive examples")

            # Assumes the dataset is sorted by question_id!
            group_train = train_tmp.groupby(["question_id"]).size().values
            group_valid = valid_tmp.groupby(["question_id"]).size().values

            lgb_train = lgb.Dataset(X_train, label=y_train, group=group_train)
            lgb_valid = lgb.Dataset(X_valid, label=y_valid, group=group_valid)

            valid_sets = [lgb_train, lgb_valid]

            model_lgb = lgb.train(
                params=self.param,
                train_set=lgb_train,
                num_boost_round=3000,
                valid_sets=valid_sets,
                early_stopping_rounds=300,
                verbose_eval=50,
            )
            self.model_lgb = model_lgb

    def predict(self, candidates: pd.DataFrame) -> np.array:
        """Predict

        Args:
            candidates (pd.DataFrame): candidates

        Returns:
            np.array: preds
        """
        return self.model_lgb.predict(candidates[self.X_cols])
