import pandas as pd
import plotnine as pn
from mizani.formatters import percent_format


def plot_previous_answer_count(questions: pd.DataFrame) -> pn.ggplot:
    """We want to know how many question a user has answered before answering the
    current one. It will give us hint at knowing how strong is the cold start problem.

    Args:
        questions (pd.DataFrame): questions

    Returns:
        pn.ggplot: plot
    """
    previous_answer_count = (
        questions.sort_values("date_answer_date")
        .eval("count = 1")
        .groupby(["user_id", "date_answer_date"])["count"]
        .sum()
        .reset_index()
    )
    previous_answer_count["previous_answer_count"] = previous_answer_count.groupby(
        ["user_id"]
    )["count"].cumsum()
    previous_answer_count["previous_answer_count"] = (
        previous_answer_count.groupby("user_id")["previous_answer_count"]
        .shift(-1)
        .fillna(0)
    )
    previous_answer_count["previous_answer_count"] = previous_answer_count[
        "previous_answer_count"
    ]
    previous_answer_count = (
        previous_answer_count.groupby("previous_answer_count")["count"]
        .sum()
        .reset_index()
    )
    previous_answer_count["cumsum_pct"] = (
        previous_answer_count["count"].cumsum() / previous_answer_count["count"].sum()
    )
    previous_answer_count = previous_answer_count.sort_values("previous_answer_count")

    plot = (
        pn.ggplot(
            previous_answer_count.head(100),
            pn.aes("previous_answer_count", "cumsum_pct"),
        )
        + pn.geoms.geom_line()
        + pn.scale_y_continuous(labels=percent_format(), limits=(0, 1))
        + pn.labs(
            x="Number of questions answered by the user BEFORE the current question",
            y="Cumulative % of questions",
            title="A significant part of questions are answered by new users.\n"
            "We have a strong cold start problem\n"
            "Note: Dataset is left censored, cold start is over-estimated",
        )
    )
    return plot


def plot_recall_by_candidates(valid: pd.DataFrame) -> pn.ggplot:
    recall_candidates_generation = (
        valid.groupby("pred_rank")["target"].sum().reset_index()
    )

    recall_candidates_generation["target"] = (
        recall_candidates_generation["target"].cumsum() / valid["question_id"].nunique()
    )
    recall_candidates_generation = recall_candidates_generation.rename(
        columns={
            "target": "recall",
            "pred_rank": "K",
        }
    )
    plot = (
        pn.ggplot(recall_candidates_generation, pn.aes("K", "recall"))
        + pn.geoms.geom_line()
        + pn.scale_y_continuous(labels=percent_format(), limits=(0, 1))
        + pn.labs(
            y="Recall (higher the better)",
            x="K - Number of candidates generated with first model \n"
            "(lower the better)",
            title="According to this graph we can know if we should add more candidates",
        )
    )
    return plot
