FROM python:3.7.6

RUN apt-get update \
    && apt-get install --no-install-recommends --yes \
    # required for LightGBM
    libgomp1="8.3.0-6" \
    && rm -rf /var/lib/apt/lists/*

COPY run_pipeline.py ./
COPY src ./src
COPY requirements.txt ./
RUN pip install -r requirements.txt

# sales_forecasting was developed is an autonomous sub-project and need to be ran 
# inside its directory to act as workdir. Should be challenged.
CMD ["sh", "-c", "python run_pipeline.py --pipeline='train_predict'"]
