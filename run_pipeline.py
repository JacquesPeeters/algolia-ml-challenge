import argparse

import pandas as pd

import src.analysis as analysis
import src.constants as constants
import src.metrics as metrics
import src.ml_utils as ml_utils
import src.model as model
import src.path as path
import src.preprocessing as prep
from src.logger import LOGGER


def run_pipeline_analysis() -> None:
    """Run the pipeline which includes various steps for developement/analytics
    purpose:
    * compute various plot to better understand the problem
    * benchmark different models
    """

    LOGGER.info(f"CURRENT_ENVIRONMENT: {constants.CURRENT_ENVIRONMENT}")

    LOGGER.info("Read data & preprocessing")
    questions = prep.read_questions()

    # For exploratory data analysis I like to use pandas-profiling but
    # it would be over-kill here
    LOGGER.info(f"Number of unique question_id: {questions['question_id'].nunique()}")
    LOGGER.info(f"Number of unique user_id: {questions['user_id'].nunique()}")

    plot = analysis.plot_previous_answer_count(questions)
    plot.save(path.path_artifact("plot_previous_answer_count.png"))

    models_to_benchmark = [
        model.TFIDF(N_CANDIDATES=20),
        model.TFIDF_SVD(N_CANDIDATES=20),
        model.TFIDF_SVD_LGBM(),
        model.SentenceTransformer(20),
        model.SentenceTransformer_LGBM(),
    ]

    for my_model in models_to_benchmark:
        LOGGER.info(f"Benchmark model:{my_model.model_name}")
        valid = prep.get_valid_and_predict(questions, my_model)
        metrics.log_metrics(my_model.model_name, valid)
        if hasattr(my_model, "second_model"):
            if hasattr(my_model.second_model, "model_lgb"):
                importance = ml_utils.get_importance_lgb(
                    my_model.second_model.model_lgb
                )
                plot = ml_utils.plot_importance_lgb(importance)
                plot.save(path.path_artifact("plot_importance_lgb.png"))

    experiments = metrics.get_previous_experiments(len(models_to_benchmark))
    experiments.to_csv(path.path_artifact("benchmark_experiments.csv"), index=False)

    # Debug we want to know what is the bottleneck and answer the question:
    # Is it worth adding more candidates?
    my_model = model.TFIDF_SVD(N_CANDIDATES=2_000)
    valid = prep.get_valid_and_predict(questions, my_model)
    plot = analysis.plot_recall_by_candidates(valid)
    plot.save(path.path_artifact("plot_recall_by_candidates.png"))


def run_pipeline_train_predict() -> None:
    """Run the pipeline which includes:
    * training the desired algorithm
    * predict the test questions
    """
    LOGGER.info(f"CURRENT_ENVIRONMENT: {constants.CURRENT_ENVIRONMENT}")

    LOGGER.info("Read data & preprocessing")
    questions = prep.read_questions()
    questions_test = prep.read_questions_test()

    LOGGER.info("Train model")
    my_model = model.SentenceTransformer_LGBM()
    my_model.train(questions)

    LOGGER.info("Predict questions_test")
    # Here assume this the day following the last one from historical questions
    date_to_predict = questions["date_answer_date"].max() + pd.Timedelta(days=1)

    questions_test["date_answer_date"] = date_to_predict
    submission = my_model.predict(questions_test, questions)
    submission = prep.format_submission(submission)
    submission.to_csv(path.path_artifact("submission.csv"), index=False)


d_pipeline = {
    "train_predict": run_pipeline_train_predict,
    "analysis": run_pipeline_analysis,
}


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-p",
        "--pipeline",
        type=str,
        dest="pipeline_name",
        help="Pipeline to execute",
        required=True,
    )
    args, _ = parser.parse_known_args()
    pipeline_name = args.pipeline_name

    d_pipeline[pipeline_name]()
